package com.example.integration;

import com.intuit.karate.junit5.Karate;

class UserIT {
    
    @Karate.Test
    Karate testUsers() {
        return Karate.run("classpath:spec/")
               .outputCucumberJson(true)
//                .outputJunitXml(true)
                .relativeTo(getClass());
    }    

}

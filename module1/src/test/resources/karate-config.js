function fn() {
  var env = karate.env; // get system property 'karate.env'
  karate.log('karate.env system property was:', env);
  if (!env) {
    env = 'dev';
  }
  var config = {
    env: env,
    myVarName: 'someValue'
  }
  if (env == 'dev') {
    // customize
    // e.g. config.foo = 'bar';
  } else if (env == 'e2e') {
    // customize
  }
    // Configure Karate to output JSON and XML reports
    karate.configure('report', {
      showLog: true,
      showAllSteps: false,
      format: ['json', 'junit'] // specify the formats
    });
  return config;
}